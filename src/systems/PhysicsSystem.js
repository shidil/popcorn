import Artemis from 'artemisjs';

let extendSystem = Artemis.ExtendHelper.extendSystem;
let baseClass = Artemis.entityProcessingSystem;
let systemType = 'physicsSystem';
let componentTypes = ['positionComponent', 'physicsComponent'];

const PhysicsSystem = extendSystem(baseClass, systemType, {
  __componentTypes: componentTypes,
  create: function () {
    let aspect = Artemis.Aspect.getAspectForAll.apply(null, this.__componentTypes);
    let self = Artemis.entityProcessingSystem.create.call(this, aspect);
    return self;
  },
  processEntity: function (e) {
    let position = this.positionComponentMapper.get(e);
    let physics = this.physicsComponentMapper.get(e);

    position.x = physics.body.p.x;
    position.y = physics.body.p.y;
  }
});

export default PhysicsSystem;
