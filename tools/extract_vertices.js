var fs = require('fs');
var plist = require('plist');
var path = require('path');

var readFile = function (path) {
  var content = null;
  try {
    content = fs.readFileSync(path, 'utf8');
  } catch(e) {
    throw e;
  }
  return content;
};

var plistToJson = function (data) {
  var jsonData = null;
  try {
    jsonData = plist.parse(data);
  } catch(e) {
    throw e;
  }
  return jsonData;
};

var extractVertices = function (data) {
  var vertices = [];
  var fixtures = data.fixtures || [];

  var convertVertices = function (polygon) {
   return polygon.map(function(vertex) {
     return vertex.match(/[0-9]+.[0-9]+/g).map(Number);
   })
  };

  var flattenVertices = function (polygon) {
    var flatVertices = [];

    for(var i = 0; i < polygon.length; i++) {
      var vertex = polygon[i];
      flatVertices.push(vertex[0]);
      flatVertices.push(vertex[1]);
    }
    return flatVertices;
  };

  for (var i = fixtures.length - 1; i >= 0; i--) {
    var polygons = fixtures[i].polygons;
    polygons = polygons.map(convertVertices);
    polygons = polygons.map(flattenVertices);
    vertices = vertices.concat(polygons);
  }

  return vertices;
};

var processFiles = function (files) {
  files = files || [];
  var len = files.length;

	if (files.length > 2) {
  	for (var i = 2; i < len; i = i + 1) {
      var file = files[i];
    	console.log(`${i - 1}. processing ${file}`);

    	var content = readFile(file);
      var data = plistToJson(content);
      // extract vertices data only
      var bodies = Object.keys(data.bodies);
      for (var j = bodies.length - 1; j >= 0; j = j - 1) {
        var bodyName = bodies[j];
        output = {};
        output.name = bodyName;
        output.vertices = extractVertices(data.bodies[bodyName]);

        // write output
        var contentOut = JSON.stringify(output, null, 2);
        var fileName = `${i - 1}-${bodyName}.json`;
        fs.writeFileSync(path.resolve(file, '../../../popcorn/src/beakers/' + fileName), contentOut, 'utf-8');
        //console.log(contentOut);
      }
  	}
	} else {
    console.error('No arguments passed. usage node <bin> file1 file2 ...');
  }
};

processFiles(process.argv);
