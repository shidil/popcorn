var fs = require('fs');
var plist = require('plist');

if (process.argv && process.argv.length > 2) {
  var len = process.argv.length
  for (var i = 2; i < len; i = i + 1) {
    console.log(`${i - 1}. processing ${process.argv[i]}`);
    try {
      var plistString = fs.readFileSync(process.argv[i], 'utf8');
      var data = plist.parse(plistString);
      fs.writeFileSync(`${i - 1}.json`, JSON.stringify(data), 'utf8');
    }
    catch(e) {
      console.error(e);
    }
  }
} else {
  console.error('No arguments passed. usage node <bin> file1 file2 ...');
}
