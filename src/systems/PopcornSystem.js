import Artemis from 'artemisjs';

let extendSystem = Artemis.ExtendHelper.extendSystem;
let baseClass = Artemis.entityProcessingSystem;
let systemType = 'popcornSystem';
let componentTypes = ['delayComponent', 'physicsComponent'];
let context = {};

const PopcornSystem = extendSystem(baseClass, systemType, {
  __componentTypes: componentTypes,
  create: function (factory) {
    let aspect = Artemis.Aspect.getAspectForAll.apply(null, this.__componentTypes);
    let self = Artemis.entityProcessingSystem.create.call(this, aspect);
    context.factory = factory;
    return self;
  },
  processEntity: function (e) {
    let delayComponent = this.delayComponentMapper.get(e);
    delayComponent.counter = delayComponent.counter + this.getWorld().getDelta();

    // if delay expires pop the popcorn
    if (delayComponent.delay <= delayComponent.counter) {
      let body = this.physicsComponentMapper.get(e).body;
      context.factory.replaceDynamicShape(body, 'circle', {
        radius: 25
      });
    }
  }
});

export default PopcornSystem;
