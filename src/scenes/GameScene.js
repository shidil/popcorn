import {CC, CP} from '../constants/globals';
import PhysicsFactory from '../Objects/PhysicsFactory';
import Artemis from 'artemisjs';
import PositionComponent from '../components/PositionComponent';
import PhysicsComponent from '../components/PhysicsComponent';
import DelayComponent from '../components/DelayComponent';
import PhysicsSystem from '../systems/PhysicsSystem';
import PopcornSystem from '../systems/PopcornSystem';
import Beaker from '../beakers/1-beaker.json';

const gameStates = {
  loading: 0,
  paused: 1,
  running: 2,
  over: 3
};

const GameLayer = CC.Layer.extend({
  stateTime: 0,
  state: gameStates.loading,
  popcorns: [],
  physicsFactory: null,
  PMR: 1,
  debug: true,
  ecs: null,
  world: null,
  maxPopcorns: 100,

  ctor: function () {
    this._super();
    this.initPhysics();
    this.initEntitySystem();
    this.initScene();
    this.scheduleUpdate();
    return true;
  },

  initEntitySystem: function () {
    this.ecs = Artemis.world.create();
    this.ecs.setSystem(PhysicsSystem.create());
    this.ecs.setSystem(PopcornSystem.create(this.physicsFactory));
    this.ecs.initialize();
  },

  initScene: function () {
    let size = CC.winSize;
    let center = CC.p(size.width / 2, size.height / 2);
    let levelLabel = new CC.LabelTTF('Level 1', 'Arial', 38);

    // position the label on the center of the screen
    levelLabel.x = center.x;
    levelLabel.y = center.y * 2 - 100;

    let jar = this.createJar(Beaker.vertices, CC.p(20, 50));
    jar.addToWorld();

     // Handle Touch
    CC.eventManager.addListener({
      event: CC.EventListener.TOUCH_ALL_AT_ONCE,
      onTouchesMoved: this.addPopcorns.bind(this)
    }, this);

    this.state = gameStates.running;
    this.addChild(levelLabel, 0);
  },

  addPopcorns: function (touch, e) {
    if (this.popcorns.length < this.maxPopcorns) {
      let pos = e.getTouches()[0].getLocation();
      let popcorn = this.createpopcorn(pos);
      this.popcorns.push(popcorn);
      popcorn.addToWorld();
    }
  },

  createpopcorn: function (pos) {
    let popcorn = this.ecs.createEntity();
    popcorn.addComponent(new PositionComponent(pos.x, pos.y));
    popcorn.addComponent(new DelayComponent(4));
    popcorn.addComponent(new PhysicsComponent(this.physicsFactory, {
      type: 'dynamic',
      shape: 'circle',
      radius: 15,
      friction: 0,
      elasticity: 0,
      density: 10,
      pos: pos
    }));

    return popcorn;
  },

  createJar: function (vertices, pos) {
    let jar = this.ecs.createEntity();
    jar.addComponent(new PositionComponent(pos.x, pos.y));
    jar.addComponent(new PhysicsComponent(this.physicsFactory, {
      type: 'static',
      shape: 'polygon',
      friction: 0,
      elasticity: 0,
      vertices: vertices,
      pos: pos
    }));

    return jar;
  },

  initPhysics: function () {
    this.physicsFactory = new PhysicsFactory({
      pmr: this.PMR,
      gravity: CP.v(0, -980),
      continues: true
    });
    this.world = this.physicsFactory.getWorld();

    if (this.debug) {
      let debugNode = new CC.PhysicsDebugNode(this.world);
      debugNode.visible = true;
      this.addChild(debugNode);
    }
  },

  update: function (dt) {
    this.stateTime = this.stateTime + dt;
    if (this.state === gameStates.paused) {
      return;
    }

    this.ecs.setDelta(dt);
    this.ecs.process();

    // update physics
    this.world.step(dt);
  }
});

const GameScene = CC.Scene.extend({
  onEnter: function () {
    this._super();
    let layer = new GameLayer();
    this.addChild(layer);
  }
});

export default GameScene;
