export default class PositionComponent {
  constructor(x, y) {
    this.x = x;
    this.y = y;

    Object.defineProperty(this, 'componentType',
      { value: 'positionComponent' }
    );
  }
}
