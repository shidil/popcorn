/**
 * @author Shidil Eringa
 */
const Resources = {
  logoBig: 'res/logo.png',
  ball: 'res/ball.png',
  ground: 'res/ground.png',
  beakerJSON: 'res/beakers/1-beaker.json'
};

/**
 * @description Forms urls of resources to load.
 * @return {object} url array
 */
Resources.getResourceMeta = () => {
  let gameResourcesToLoad = [];
  for (let i in Resources) {
    if (Resources.hasOwnProperty(i)) {
      gameResourcesToLoad.push(Resources[i]);
    }
  }
};

export default Resources;
