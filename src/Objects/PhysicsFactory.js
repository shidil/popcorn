import {CC, CP} from '../constants/globals';

const bodyMoments = {
  circle: CP.momentForCircle,
  box: CP.momentForBox,
  polygon: CP.momentForPoly,
  static: Infinity
};

class PhysicsFactory {
  /**
   * Physics factory constructor
   * @constructs PhysicsFactory
   * @param {Object} config physics options
   * @return {undefined}
   */
  constructor(config) {
    this.world = new CP.Space();
    this.pmr = config.pmr;
    this.world.gravity = config.gravity;
    this.world.sleepTimeThreshold = 1;
  }

  /**
   * World Getter
   * @return {Object} world object
   */
  getWorld() {
    return this.world;
  }

  /**
   * World Setter
   * @param {Object} world world object to set
   * @return {undefined}
   */
  setWorld(world) {
    this.world = world;
  }

  getBodyMoment(shape = 'circle') {
    return bodyMoments[shape];
  }

  createBodyMoment(options) {
    let moment = this.getBodyMoment(options.shape);
    if (options.shape === 'circle') {
      moment = moment(options.density || 1, 0, options.radius, CP.v(0, 0));
    } else if (options.shape === 'box') {
      moment = moment(options.density, options.width, options.height);
    }

    return moment;
  }

  createBodyDef(options) {
    let bodyDef = null;

    // for static bodies
    if (options.type === 'static') {
      bodyDef = {
        density: Infinity,
        moment: Infinity
      };
    } else {
      bodyDef = {
        density: options.density || 1,
        moment: this.createBodyMoment(options)
      };
    }

    return bodyDef;
  }

  // createCircleShape(body, options) {

  // }

  createShape(body, type = 'circle', options = {}) {
    let finalShape = null;

    if (type === 'circle') {
      finalShape = new CP.CircleShape(body, options.radius / this.pmr, CP.v(0, 0));
      finalShape.setElasticity(options.elasticity || 0);
      finalShape.setFriction(options.friction || 0);
    } else if (type === 'box') {
      let width = options.width / this.pmr;
      let height = options.height / this.pmr;

      finalShape = new CP.BoxShape(body, width, height);
      finalShape.setElasticity(options.elasticity || 0);
      finalShape.setFriction(options.friction || 0);
    } else if (type === 'polygon') {
      finalShape = [];
      for (let i = options.vertices.length - 1; i >= 0; i = i - 1) {
        let shape = new CP.PolyShape(body, options.vertices[i], CP.v(0, 0));
        shape.setElasticity(options.elasticity || 0);
        shape.setFriction(options.friction || 0);
        finalShape.push(shape);
      }
    }

    return finalShape;
  }

  /**
   * Removed a shape from a body and attaches specified new shape
   * Limitation: Body can have only one shape
   * @param {Object} body shape's body
   * @param {string} type shape class
   * @param {Object} options shape creation options
   * @return {undefined}
   */
  replaceDynamicShape(body, type, options) {
    body.eachShape(this.world.removeShape.bind(this.world));
    let shape = this.createShape(body, type, options);
    this.world.addShape(shape);
  }

  /**
   * Creates a box2d body with given config
   * @param {Object} config body options
   * @return {Object} box2d body object
   */
  createBody(config) {
    let bodyDef = this.createBodyDef(config);
    let body = new CP.Body(bodyDef.density, bodyDef.moment);
    let shape = this.createShape(body, config.shape, config);

    body.a = CC.degreesToRadians(config.angle || 0);
    body.setPos(config.pos);

    if (config.type === 'static') {
      body.nodeIdleTime = Infinity;
      if (shape.constructor === Array) {
        for (let i = shape.length - 1; i >= 0; i = i - 1) {
          shape[i].cacheBB();
          this.world.addStaticShape(shape[i]);
        }
      } else {
        shape.cacheBB();
        this.world.addStaticShape(shape);
      }
    } else {
      this.world.addShape(shape);
      this.world.addBody(body);
    }

    return body;
  }

}

export default PhysicsFactory;
