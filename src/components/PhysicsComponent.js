export default class PhysicsComponent {
  constructor(factory, config) {
    this.body = factory.createBody(config);

    Object.defineProperty(this, 'componentType',
      { value: 'physicsComponent' }
    );
  }
}
