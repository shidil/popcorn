import {CC} from '../constants/globals';
import Resources from '../constants/resources';
import SceneManager from '../SceneManager';

const MainMenuLayer = CC.Layer.extend({
  sprite: null,
  stateTime: 0,
  state: 1,

  ctor: function () {
    this._super();

    this.initScene();
    this.scheduleUpdate();
    this.addEventListner(CC.EventListener.KEYBOARD, {
      onKeyReleased: this.processKeyboardInput
    }, this);
    return true;
  },

  initScene: function () {
    let size = CC.winSize;
    let helloLabel = new CC.LabelTTF('Popcorn', 'Arial', 38);

    // position the label on the center of the screen
    helloLabel.x = size.width / 2;
    helloLabel.y = size.height / 2 + 200;

    // configure the logo"
    this.sprite = new CC.Sprite(Resources.logoBig);
    this.sprite.attr({
      x: size.width / 2,
      y: size.height / 2
    });

    // add items to screen
    this.addChild(helloLabel, 5);
    this.addChild(this.sprite, 0);
  },

  update: function (dt) {
    this.stateTime = this.stateTime + dt;
    if (!this.state) {
      return;
    }

    if (this.stateTime > 0.5) {
      this.state = 0;
      SceneManager.loadGameScene();
    }
  },

  addEventListner: function (event, callBacks, context) {
    let ccEvent = {};
    ccEvent.event = event;

    // map callbacks
    for (let key in callBacks) {
      if (typeof callBacks[key] === 'function') {
        ccEvent[key] = callBacks[key];
      }
    }
    CC.eventManager.addListener(ccEvent, context);
  },

  processKeyboardInput: function (code, event) {
    if (code === CC.KEY.back || code === CC.KEY.escape) {
      this.handleBackButton(event);
    }
  },

  handleBackButton: function () {
    CC.director.end();
  }
});

const MainMenuScene = CC.Scene.extend({
  onEnter: function () {
    this._super();
    let layer = new MainMenuLayer();
    this.addChild(layer);
  }
});

export default MainMenuScene;
