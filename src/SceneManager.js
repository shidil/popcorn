import {CC} from './constants/globals';
import MainMenuScene from './scenes/MainMenuScene';
import GameScene from './scenes/GameScene';

/**
 * Singleton for managing scenes.
 * @type {Object}
 */
const SceneManager = {
  /**
   * Load main menu scene at the start
   * @return {undefined}
   */
  loadInitialScene: () => {
    CC.director.runScene(new MainMenuScene());
  },

  /**
   * Hook to run the next scene. replaces old scene with new
   * @param {Object} nextScene scene to load next
   * @return {undefined}
   */
  loadNextScene: (nextScene) => {
    CC.director.runScene(nextScene);
  },

  loadGameScene: () => {
    SceneManager.loadNextScene(new GameScene());
  }
};

export default SceneManager;
