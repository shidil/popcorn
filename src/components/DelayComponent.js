export default class DelayComponent {
  constructor(delay) {
    this.delay = delay || 0;
    this.counter = 0;

    Object.defineProperty(this, 'componentType',
      { value: 'delayComponent' }
    );
  }
}
